<?php

namespace App\Http\Controllers;

use App\Financeiro;
use Illuminate\Http\Request;

class FinanceiroController extends Controller
{
    public function get()
	{
		$financeiro = Financeiro::selectRaw('SUM(valor) as valor, YEAR(data_compra) as ano, MONTH(data_compra) as mes')
						->groupBy('ano', 'mes')
						->get();
		$data = $financeiro->pluck('valor')->toArray();
		$dates = $financeiro->map(function($f) {
			return $f->mes.'/'.$f->ano;
		})->toArray();

		$pie = Financeiro::selectRaw('SUM(valor) as valor, YEAR(data_compra) as ano')
						->groupBy('ano')
						->get();
		$pieValues = $pie->pluck('valor')->toArray();
		$pieYears = $pie->map(function($f) {
			return $f->ano;
		});
		return response()->json(['data' => $data, 'legend' => $dates, 'pieValues' => $pieValues, 'pieYears' => $pieYears ], 200);
	}


	public function tabela(Request $request)
    {
        return response()->json(
			[
                'data'          => Financeiro::select('id','valor','data_compra','user_id')
									->with('user')
									->orderBy('data_compra','DESC')
									->get(),
                'columns'       => ['nome','data_compra_txt','valor_txt'],
                'columnsName'   => ['Nome','Comprado em','Preço']
			]
		);
    }
}
