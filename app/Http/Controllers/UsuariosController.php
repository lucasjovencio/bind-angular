<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UsuariosController extends Controller
{
    public function get(Request $request)
    {
        return response()->json(
            [
                'data'          => User::select('id','name','lastname','email','created_at')->get(),
                'columns'       => ['name','lastname','email','created_at','displayName'],
                'columnsName'   => ['Nome','Sobrenome','Email','Data de criação','nome completo']
            ]
        );
    }
}
