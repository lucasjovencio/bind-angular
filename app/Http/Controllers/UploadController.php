<?php

namespace App\Http\Controllers;

use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function upload(Request $request)
	{
		$path = Storage::putFile('files', $request->file);
		Upload::create(['caminho' => $path]);
		
		return response()->json(['chegou' => Upload::all()],200);
	}
}
