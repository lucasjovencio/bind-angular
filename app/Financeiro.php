<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Financeiro extends Model
{

    protected $appends = [
        'nome',
        'data_compra_txt',
        'valor_txt',
    ];
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }


    public function getNomeAttribute()
    {
        return $this->user->displayName ?? '';
    }

    public function getDataCompraTxtAttribute()
    {
        return Carbon::parse($this->data_compra)->format('d/m/Y');
    }

    public function getValorTxtAttribute()
    {
        return "R$ " . number_format(strval($this->valor),2,',','.');
    }
}
