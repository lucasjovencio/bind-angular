<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function (){
    Route::prefix('credenciais')->group(function (){
        Route::prefix('login')->group(function (){
            Route::post('/login','Auth\LoginController@authenticate');
            Route::get('/logout','Auth\LoginController@logout');
        });

        Route::prefix('cadastro')->group(function (){

        });


    });
    
});

Route::prefix('/financeiro')->group(function() {
	Route::get('/get','FinanceiroController@get');
	Route::get('/tabela','FinanceiroController@tabela');
});

Route::post('/upload','UploadController@upload');

Route::prefix('/usuarios')->group(function() {
	Route::get('/get','UsuariosController@get');
});
