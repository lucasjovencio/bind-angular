<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMensagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensagens', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->text('mensagem');
			$table->boolean('lida')->default(false);
			$table->bigInteger('de_id')->unsigned();
			$table->bigInteger('para_id')->unsigned();
			$table->foreign('de_id')->references('id')->on('users');
			$table->foreign('para_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensagens');
    }
}
