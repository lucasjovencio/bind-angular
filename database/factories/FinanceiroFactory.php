<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Financeiro;
use App\User;
use Faker\Generator as Faker;

$factory->define(Financeiro::class, function (Faker $faker) {
	$dt = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now');
	$date = now()->subMonth(2)->addDays(rand(15,250))->format('Y-m-d'); // 1994-09-24
    return [
        'data_compra'	=> $date,
		'valor'			=> $faker->randomFloat($nbMaxDecimals = 2, $min = 20, $max = 200),
		'user_id'		=> function() {
			return User::inRandomOrder()->first()->id;
		}
    ];
});
