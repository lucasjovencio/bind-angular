<?php

use App\Financeiro;
use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 100)->create();
        factory(User::class, 1)->create(['email'=>'cliente@teste.com']);
        factory(Financeiro::class, 2000)->create();
    }
}
